module.exports = {
	apps : [{
		name      : 'core/reddit-rt',
		script    : 'build/reddit-rt.js',
		env: {
			// Either inject the values via environment variables or define them here
            TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || "",
            REDDIT_RT_SUBREDDIT: process.env.REDDIT_RT_SUBREDDIT || "",
			REDDIT_RT_USE_POSTS: process.env.REDDIT_RT_USE_POSTS || true,
			REDDIT_RT_POSTS_ALLSUBREDDITS: process.env.REDDIT_RT_POSTS_ALLSUBREDDITS || false,
			REDDIT_RT_MODLOG_INTERVAL: process.env.REDDIT_RT_MODLOG_INTERVAL || 2*60*1000,
			REDDIT_RT_DB_DIALECT: process.env.REDDIT_RT_DB_DIALECT || undefined,
			REDDIT_RT_DB_HOST: process.env.REDDIT_RT_DB_HOST || undefined,
			REDDIT_RT_DB_NAME: process.env.REDDIT_RT_DB_NAME || undefined,
			REDDIT_RT_DB_USERNAME: process.env.REDDIT_RT_DB_USERNAME || undefined,
			REDDIT_RT_DB_PASSWORD: process.env.REDDIT_RT_DB_PASSWORD || undefined,
			REDDIT_RT_DB_PORT: process.env.REDDIT_RT_DB_PORT || undefined,
			REDDIT_RT_DB_PATH: process.env.REDDIT_RT_DB_PATH || undefined,
			REDDIT_RT_DB_DROP_TABLES_ON_START: process.env.REDDIT_RT_DB_DROP_TABLES_ON_START || false
		}
	}]
};
