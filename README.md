# Reddit Realtime

This is a PCMRBot.js module that integrates into the Pushshift Reddit SSE stream and creates realtime streams of other Reddit services.

At the current time, it is suggested that only one instance of the Reddit RT module runs per cluster. 

## Dependencies

This module has the following dependencies: 

* [`core/reddit`](https://gitlab.com/pcmasterrace/pcmrbot.js/core/utilities), version 1

## Environment variables

The following environment variables should be set prior to launch. 

* Module options
  * `REDDIT_RT_SUBREDDIT` - The name of the subreddit to watch
  * (optional) `REDDIT_RT_USE_POSTS` - Whether to enable the Posts RT module. Defaults to false
  * (optional) `REDDIT_RT_POSTS_ALLSUBREDDITS` - Whether the Posts RT module should emit events for all subreddits, not just the specified one. **WARNING** - Depending on Reddit traffic, this will drastically increase network utilization and CPU load. Defaults to false
  * (optional) `REDDIT_RT_USE_MODLOG` - Whether to enable the Modlog RT module. Defaults to false
  * (optional) `REDDIT_RT_MODLOG_INTERVAL` - Interval between modlog checks, in milliseconds. Defaults to `60000` (60 seconds)
  * (optional) `REDDIT_RT_USE_MODQUEUE` - Whether to enable the Modqueue RT module. Defaults to false
  * (optional) `REDDIT_RT_MODQUEUE_INTERVAL` - Interval between modqueue checks, in milliseconds. Defaults to `60000` (60 seconds)
* Database options ([options passed directly into Sequelize](http://docs.sequelizejs.com/manual/installation/usage.html))
  * `REDDIT_RT_DB_DIALECT` - The database dialect to use
  * `REDDIT_RT_DB_HOST` - The database hostname to connect to
  * `REDDIT_RT_DB_NAME` - The database name to connect to
  * `REDDIT_RT_DB_USERNAME` - The database username
  * `REDDIT_RT_DB_PASSWORD` - The database password
  * `REDDIT_RT_DB_PORT` - The database port
  * `REDDIT_RT_DB_PATH` - (SQLite only) The database path on the filesystem
  * (optional) `REDDIT_RT_DB_DROP_TABLES_ON_START` - Whether the tables used should be cleaned at launch. Defaults to false
* Broker options
  * (optional) `TRANSPORT_NODE_ID` - The node ID that should be used for connecting to the rest of the cluster. Defaults to the system hostname plus a random number string. 
  * (optional) `TRANSPORT_BIND_ADDRESS` - The network address that the service broker should bind to. Defaults to all available addresses across all network interfaces.

# API Reference 

## Modules

<dl>
<dt><a href="#module_reddit-rt.db">reddit-rt.db</a></dt>
<dd><p>This module acts as the database layer for storing state within this service. This module is not meant to be publicly available, but is documented here for completeness.</p>
</dd>
<dt><a href="#module_reddit-rt.modlog">reddit-rt.modlog</a></dt>
<dd><p>This module emits events whenever new modlog entries are created on Reddit. Event names follow the format <code>reddit-rt.modlog.&lt;name&gt;</code>, where <code>&lt;name&gt;</code> is one of <code>banuser</code>, <code>unbanuser</code>, <code>spamlink</code>, <code>removelink</code>, <code>approvelink</code>, <code>spamcomment</code>, <code>removecomment</code>, <code>approvecomment</code>, <code>addmoderator</code>, <code>invitemoderator</code>, <code>uninvitemoderator</code>, <code>acceptmoderatorinvite</code>, <code>removemoderator</code>, <code>addcontributor</code>, <code>removecontributor</code>, <code>editsettings</code>, <code>editflair</code>, <code>distinguish</code>, <code>marknsfw</code>, <code>wikibanned</code>, <code>wikicontributor</code>, <code>wikiunbanned</code>, <code>wikipagelisted</code>, <code>removewikicontributor</code>, <code>wikirevise</code>, <code>wikipermlevel</code>, <code>ignorereports</code>, <code>unignorereports</code>, <code>setpermissions</code>, <code>setsuggestedsort</code>, <code>sticky</code>, <code>unsticky</code>, <code>setcontestmode</code>, <code>unsetcontestmode</code>, <code>lock</code>, <code>unlock</code>, <code>muteuser</code>, <code>unmuteuser</code>, <code>createrule</code>, <code>editrule</code>, <code>deleterule</code>, <code>spoiler</code>, <code>unspoiler</code>, <code>modmail_enrollment</code>, <code>community_styling</code>, <code>community_widgets</code>, <code>markoriginalcontent</code> and the payload being the modlog item details.</p>
<p>While the <code>checkModlog</code> action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.</p>
</dd>
<dt><a href="#module_reddit-rt.modqueue">reddit-rt.modqueue</a></dt>
<dd><p>This module emits events whenever items are added to the modqueue, updated (reports added), or removed. These events are <code>reddit-rt.modqueue.newEntry</code>, <code>reddit-rt.modqueue.updatedEntry</code>, and <code>reddit-rt.modqueue.removedEntry</code> respectively with the payload being the modqueue item details.</p>
<p>While the <code>checkModqueue</code> action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.</p>
</dd>
<dt><a href="#module_reddit-rt.posts">reddit-rt.posts</a></dt>
<dd><p>This module emits events whenever new comments or submissions are published on the specified subreddit. The events emitted are <code>reddit-rt.posts.comment</code> and <code>reddit-rt.posts.submission</code>, with the payload being the post details.</p>
<p>This uses the Pushshift SSE stream as the data source, meaning that this module will not consume Reddit API calls.</p>
</dd>
</dl>

<a name="module_reddit-rt.db"></a>

## reddit-rt.db
This module acts as the database layer for storing state within this service. This module is not meant to be publicly available, but is documented here for completeness.

**Version**: 1  

* [reddit-rt.db](#module_reddit-rt.db)
    * [.getLatestModAction()](#module_reddit-rt.db.getLatestModAction)
    * [.updateModAction(action, timestamp, modAction)](#module_reddit-rt.db.updateModAction)
    * [.getOldModqueue()](#module_reddit-rt.db.getOldModqueue)
    * [.addModqueueEntry(name, userReports, modReports)](#module_reddit-rt.db.addModqueueEntry)
    * [.updateModqueueEntry(name, userReports, modReports)](#module_reddit-rt.db.updateModqueueEntry)
    * [.removeModqueueEntry(name)](#module_reddit-rt.db.removeModqueueEntry)

<a name="module_reddit-rt.db.getLatestModAction"></a>

### reddit-rt.db.getLatestModAction()
Gets the latest logged mod action from the database. Used by `reddit-rt.modlog`

**Kind**: static method of [<code>reddit-rt.db</code>](#module_reddit-rt.db)  
**Access**: package  
<a name="module_reddit-rt.db.updateModAction"></a>

### reddit-rt.db.updateModAction(action, timestamp, modAction)
Logs a mod action to the database. Used by `reddit-rt.modlog`

**Kind**: static method of [<code>reddit-rt.db</code>](#module_reddit-rt.db)  
**Access**: package  

| Param | Type | Description |
| --- | --- | --- |
| action | <code>string</code> | The type of action to update. One of `banuser`, `unbanuser`, `spamlink`, `removelink`, `approvelink`, `spamcomment`, `removecomment`, `approvecomment`, `addmoderator`, `invitemoderator`, `uninvitemoderator`, `acceptmoderatorinvite`, `removemoderator`, `addcontributor`, `removecontributor`, `editsettings`, `editflair`, `distinguish`, `marknsfw`, `wikibanned`, `wikicontributor`, `wikiunbanned`, `wikipagelisted`, `removewikicontributor`, `wikirevise`, `wikipermlevel`, `ignorereports`, `unignorereports`, `setpermissions`, `setsuggestedsort`, `sticky`, `unsticky`, `setcontestmode`, `unsetcontestmode`, `lock`, `unlock`, `muteuser`, `unmuteuser`, `createrule`, `editrule`, `deleterule`, `spoiler`, `unspoiler`, `modmail_enrollment`, `community_styling`, `community_widgets`, `markoriginalcontent` |
| timestamp | <code>number</code> | The timestamp of the action performed, in milliseconds (Reddit API response is in seconds) |
| modAction | <code>string</code> | The ID of the specified action as returned by Reddit |

<a name="module_reddit-rt.db.getOldModqueue"></a>

### reddit-rt.db.getOldModqueue()
Gets the contents of the modqueue as logged in the database (as opposed to the modqueue as returned by Reddit). Used by `reddit-rt.modqueue`

**Kind**: static method of [<code>reddit-rt.db</code>](#module_reddit-rt.db)  
**Access**: package  
<a name="module_reddit-rt.db.addModqueueEntry"></a>

### reddit-rt.db.addModqueueEntry(name, userReports, modReports)
Adds a new modqueue entry to the database. Used by `reddit-rt.modqueue`

**Kind**: static method of [<code>reddit-rt.db</code>](#module_reddit-rt.db)  
**Access**: package  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The fullname of the reported item |
| userReports | <code>number</code> | The number of user reports the item has |
| modReports | <code>number</code> | The number of mod reports the item has |

<a name="module_reddit-rt.db.updateModqueueEntry"></a>

### reddit-rt.db.updateModqueueEntry(name, userReports, modReports)
Updates a modqueue entry in the database. Used by `reddit-rt.modqueue`

**Kind**: static method of [<code>reddit-rt.db</code>](#module_reddit-rt.db)  
**Access**: package  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The fullname of the reported item |
| userReports | <code>number</code> | The number of user reports the item has |
| modReports | <code>number</code> | The number of mod reports the item has |

<a name="module_reddit-rt.db.removeModqueueEntry"></a>

### reddit-rt.db.removeModqueueEntry(name)
Removes a modqueue entry from the database. Used by `reddit-rt.modqueue`

**Kind**: static method of [<code>reddit-rt.db</code>](#module_reddit-rt.db)  
**Access**: package  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The fullname of the reported item |

<a name="module_reddit-rt.modlog"></a>

## reddit-rt.modlog
This module emits events whenever new modlog entries are created on Reddit. Event names follow the format `reddit-rt.modlog.<name>`, where `<name>` is one of `banuser`, `unbanuser`, `spamlink`, `removelink`, `approvelink`, `spamcomment`, `removecomment`, `approvecomment`, `addmoderator`, `invitemoderator`, `uninvitemoderator`, `acceptmoderatorinvite`, `removemoderator`, `addcontributor`, `removecontributor`, `editsettings`, `editflair`, `distinguish`, `marknsfw`, `wikibanned`, `wikicontributor`, `wikiunbanned`, `wikipagelisted`, `removewikicontributor`, `wikirevise`, `wikipermlevel`, `ignorereports`, `unignorereports`, `setpermissions`, `setsuggestedsort`, `sticky`, `unsticky`, `setcontestmode`, `unsetcontestmode`, `lock`, `unlock`, `muteuser`, `unmuteuser`, `createrule`, `editrule`, `deleterule`, `spoiler`, `unspoiler`, `modmail_enrollment`, `community_styling`, `community_widgets`, `markoriginalcontent` and the payload being the modlog item details.While the `checkModlog` action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.

**Version**: 1  
<a name="module_reddit-rt.modlog.checkModlog"></a>

### reddit-rt.modlog.checkModlog()
Retrieves updates to the modlog on Reddit and emits events as needed.

**Kind**: static method of [<code>reddit-rt.modlog</code>](#module_reddit-rt.modlog)  
**Access**: package  
<a name="module_reddit-rt.modqueue"></a>

## reddit-rt.modqueue
This module emits events whenever items are added to the modqueue, updated (reports added), or removed. These events are `reddit-rt.modqueue.newEntry`, `reddit-rt.modqueue.updatedEntry`, and `reddit-rt.modqueue.removedEntry` respectively with the payload being the modqueue item details.While the `checkModqueue` action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.

**Version**: 1  
<a name="module_reddit-rt.modqueue.checkModqueue"></a>

### reddit-rt.modqueue.checkModqueue()
Retrieves updates to the modqueue on Reddit and emits events as needed.

**Kind**: static method of [<code>reddit-rt.modqueue</code>](#module_reddit-rt.modqueue)  
**Access**: package  
<a name="module_reddit-rt.posts"></a>

## reddit-rt.posts
This module emits events whenever new comments or submissions are published on the specified subreddit. The events emitted are `reddit-rt.posts.comment` and `reddit-rt.posts.submission`, with the payload being the post details.This uses the Pushshift SSE stream as the data source, meaning that this module will not consume Reddit API calls.

**Version**: 2  
