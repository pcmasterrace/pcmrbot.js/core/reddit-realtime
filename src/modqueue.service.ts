import { Service, Context, Errors } from "moleculer";
import * as moment from "moment";
import * as yn from "yn";

/**
 * This module emits events whenever items are added to the modqueue, updated (reports added), or removed. These events are `reddit-rt.modqueue.newEntry`, `reddit-rt.modqueue.updatedEntry`, and `reddit-rt.modqueue.removedEntry` respectively with the payload being the modqueue item details.
 * 
 * While the `checkModqueue` action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.
 * 
 * @module "reddit-rt.modqueue"
 * @version 1
 */
class ModqueueService extends Service {
	protected checkInterval: number;

	constructor(broker) {
		super(broker);
		
		this.parseServiceSchema({
			name: "reddit-rt.modqueue",
			version: 1,
			dependencies: [
				{name: "reddit.modqueue", version: 1},
				{name: "reddit-rt.db", version: 1}
			],
			actions: {
				checkModqueue: {
					name: "checkModqueue",
					handler: this.checkModqueue
				}
			},
			events: {/*
				"reddit-rt.modqueue.newEntry" (msg) {
					console.log("New modqueue entry detected at", moment().format("LTS"), msg.name);
				},
				"reddit-rt.modqueue.updatedEntry" (msg) {
					console.log("Updated modqueue entry detected at", moment().format("LTS"), msg.name);
				},
				"reddit-rt.modqueue.removedEntry" (msg) {
					console.log("Modqueue entry removed from modqueue at", moment().format("LTS"), msg);
				}
			*/},
			created: this.serviceCreated,
			// @ts-ignore
			started: this.serviceStarted
		});
	}
	
	async serviceCreated() {
		this.checkInterval = Number(process.env.REDDIT_RT_MODQUEUE_INTERVAL) || 60*1000;
	}

	async serviceStarted() {
		if(yn(process.env.REDDIT_RT_USE_MODQUEUE, {default: false})) {
			setInterval(() => this.broker.call("v1.reddit-rt.modqueue.checkModqueue"), this.checkInterval);
		}
	}

	/**
	 * Retrieves updates to the modqueue on Reddit and emits events as needed. 
	 * @function
	 * @package
	 * @static
	 * @name checkModqueue
	 */
	async checkModqueue(ctx: Context) {
		// Check current saved modqueue
		let oldModqueue = await this.broker.call("v1.reddit-rt.db.getOldModqueue");
		let currentModqueue = await this.broker.call("v1.reddit.modqueue.getModqueue", {
			subreddit: process.env.REDDIT_RT_SUBREDDIT
		});
		
		let newEntries: number[] = [];
		let updatedEntries: number[] = [];
		let removedEntries: number[] = [];

		// Check to see which modqueue items are new or updated
		currentModqueue.forEach((entry, index) => {
			let oldIndex = oldModqueue.findIndex(oldEntry => oldEntry.name === entry.name)
			
			if(oldIndex < 0) {
				newEntries.push(index);
			} else {
				if(oldModqueue[oldIndex].userReports !== entry.user_reports.length ||
					oldModqueue[oldIndex].modReports !== entry.mod_reports.length) {
						updatedEntries.push(index)
					}
			}
		});

		// Check to see which modqueue items were removed;
		oldModqueue.forEach((entry, index) => {
			if(currentModqueue.findIndex(currentEntry => currentEntry.name === entry.name) < 0) {
				removedEntries.push(index);
			}
		});

		// Emit new entry alerts and add database entries
		// For loop is so that await works and doesn't break the SQLite DB
		for (let i of newEntries) {
			await this.broker.call("v1.reddit-rt.db.addModqueueEntry", {
				name: currentModqueue[i].name,
				userReports: currentModqueue[i].user_reports.length,
				modReports: currentModqueue[i].mod_reports.length
			});

			await this.broker.emit("reddit-rt.modqueue.newEntry", currentModqueue[i]);
		}

		// Emit updated entry alerts and update existing database entries
		for (let i of updatedEntries) {
			await this.broker.call("v1.reddit-rt.db.updateModqueueEntry", {
				name: currentModqueue[i].name,
				userReports: currentModqueue[i].user_reports.length,
				modReports: currentModqueue[i].mod_reports.length
			});

			await this.broker.emit("reddit-rt.modqueue.updatedEntry", currentModqueue[i]);
		}

		// Emit new entry alerts and add database entries
		for (let i of removedEntries) {
			await this.broker.call("v1.reddit-rt.db.removeModqueueEntry", {
				name: oldModqueue[i].name
			});

			await this.broker.emit("reddit-rt.modqueue.removedEntry", oldModqueue[i].name);
		}
	}
}

module.exports = ModqueueService;