import * as Sequelize from "sequelize";

module.exports = function(sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes) {
	return sequelize.define("User", {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		action: {
			type: DataTypes.STRING,
			unique: true
		},
		timestamp: {
			type: DataTypes.DATE, 
			defaultValue: new Date(0)
		},
		modAction: {
			type: DataTypes.STRING,
			defaultValue: null
		}
	});
}