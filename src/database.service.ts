import { Service, Context, Errors } from "moleculer";
import * as Sequelize from "sequelize";
import * as path from "path";
import * as moment from "moment";
import * as yn from "yn";

/**
 * This module acts as the database layer for storing state within this service. This module is not meant to be publicly available, but is documented here for completeness. 
 * 
 * @module "reddit-rt.db"
 * @version 1
 */
class DatabaseService extends Service {
	protected db: Sequelize.Sequelize;
	protected ModlogEntry: Sequelize.Model<any, any>;
	protected ModqueueEntry: Sequelize.Model<any, any>;

	constructor(broker) {
		super(broker);
		
		this.parseServiceSchema({
			name: "reddit-rt.db",
			version: 1,
			actions: {
				getLatestModAction: {
					name: "getLatestModAction",
					handler: this.getLatestModAction
				},
				updateModAction: {
					name: "updateModAction",
					params: {
						action: "string",
						timestamp: "number",
						modAction: "string"
					},
					handler: this.updateModAction
				},
				getOldModqueue: {
					name: "getOldModqueue",
					handler: this.getOldModqueue
				},
				addModqueueEntry: {
					name: "addModqueueEntry",
					params: {
						name: "string",
						userReports: "number",
						modReports: "number"
					},
					handler: this.addModqueueEntry
				},
				updateModqueueEntry: {
					name: "updateModqueueEntry",
					params: {
						name: "string",
						userReports: "number",
						modReports: "number"
					},
					handler: this.updateModqueueEntry
				},
				removeModqueueEntry: {
					name: "removeModqueueEntry",
					params: {
						name: "string"
					},
					handler: this.removeModqueueEntry
				},
			},
			created: this.serviceCreated
		});
	}

	async serviceCreated() {
		this.db = new Sequelize({
			dialect: process.env.REDDIT_RT_DB_DIALECT || undefined,
			database: process.env.REDDIT_RT_DB_NAME || undefined,
			host: process.env.REDDIT_RT_DB_HOST || undefined,
			username: process.env.REDDIT_RT_DB_USERNAME || undefined,
			password: process.env.REDDIT_RT_DB_PASSWORD || undefined,
			port: Number(process.env.REDDIT_RT_DB_PORT) || undefined,
			storage: process.env.REDDIT_RT_DB_PATH || undefined,
			operatorsAliases: false,
			logging: false
		});

		this.ModlogEntry = this.db.import(path.join(__dirname, "./modlogEntry.model"));
		this.ModqueueEntry = this.db.import(path.join(__dirname, "./modqueueEntry.model"));

		await this.db.sync({force: yn(process.env.REDDIT_RT_DB_DROP_TABLES_ON_START, {default: false})});
	}

	async initializeModActionTable() {
		try {
			await this.ModlogEntry.bulkCreate([
				{action: `banuser`},
				{action: `unbanuser`},
				{action: `spamlink`},
				{action: `removelink`},
				{action: `approvelink`},
				{action: `spamcomment`},
				{action: `removecomment`},
				{action: `approvecomment`},
				{action: `addmoderator`},
				{action: `invitemoderator`},
				{action: `uninvitemoderator`},
				{action: `acceptmoderatorinvite`},
				{action: `removemoderator`},
				{action: `addcontributor`},
				{action: `removecontributor`},
				{action: `editsettings`},
				{action: `editflair`},
				{action: `distinguish`},
				{action: `marknsfw`},
				{action: `wikibanned`},
				{action: `wikicontributor`},
				{action: `wikiunbanned`},
				{action: `wikipagelisted`},
				{action: `removewikicontributor`},
				{action: `wikirevise`},
				{action: `wikipermlevel`},
				{action: `ignorereports`},
				{action: `unignorereports`},
				{action: `setpermissions`},
				{action: `setsuggestedsort`},
				{action: `sticky`},
				{action: `unsticky`},
				{action: `setcontestmode`},
				{action: `unsetcontestmode`},
				{action: `lock`},
				{action: `unlock`},
				{action: `muteuser`},
				{action: `unmuteuser`},
				{action: `createrule`},
				{action: `editrule`},
				{action: `deleterule`},
				{action: `spoiler`},
				{action: `unspoiler`},
				{action: `modmail_enrollment`},
				{action: `community_styling`},
				{action: `community_widgets`},
				{action: `markoriginalcontent`}
			]);
		} catch (err) {
			this.logger.error("Error occurred while initializing modlog database", err.name, err.message);
		}
	}

	/**
	 * Gets the latest logged mod action from the database. Used by `reddit-rt.modlog`
	 * @function
	 * @package
	 * @static
	 * @name getLatestModAction
	 */
	async getLatestModAction(ctx: Context) {
		let latestTimestamp = await this.ModlogEntry.max("timestamp");

		// First run, no records exist
		if (latestTimestamp === null) {
			await this.initializeModActionTable();
			return null;
		
		// Database schema already set up for some reason but no data is present
		} else if (latestTimestamp === moment(0).valueOf()) {
			return null;
		}

		let latest = await this.ModlogEntry.findOne({where: {timestamp: latestTimestamp}});

		return latest.modAction;
	}

	/**
	 * Logs a mod action to the database. Used by `reddit-rt.modlog`
	 * @function
	 * @package
	 * @static
	 * @name updateModAction
	 * @param {string} action - The type of action to update. One of `banuser`, `unbanuser`, `spamlink`, `removelink`, `approvelink`, `spamcomment`, `removecomment`, `approvecomment`, `addmoderator`, `invitemoderator`, `uninvitemoderator`, `acceptmoderatorinvite`, `removemoderator`, `addcontributor`, `removecontributor`, `editsettings`, `editflair`, `distinguish`, `marknsfw`, `wikibanned`, `wikicontributor`, `wikiunbanned`, `wikipagelisted`, `removewikicontributor`, `wikirevise`, `wikipermlevel`, `ignorereports`, `unignorereports`, `setpermissions`, `setsuggestedsort`, `sticky`, `unsticky`, `setcontestmode`, `unsetcontestmode`, `lock`, `unlock`, `muteuser`, `unmuteuser`, `createrule`, `editrule`, `deleterule`, `spoiler`, `unspoiler`, `modmail_enrollment`, `community_styling`, `community_widgets`, `markoriginalcontent`
	 * @param {number} timestamp - The timestamp of the action performed, in milliseconds (Reddit API response is in seconds)
	 * @param {string} modAction - The ID of the specified action as returned by Reddit
	 */
	async updateModAction(ctx: Context) {
		let dbEntry = await this.ModlogEntry.findOne({where: {action: ctx.params.action}});

		if (moment.utc(ctx.params.timestamp * 1000).isSameOrBefore(moment.utc(dbEntry.timestamp))) { 
			return false
		}

		try {
			await this.ModlogEntry.update({
				timestamp: ctx.params.timestamp * 1000,
				modAction: ctx.params.modAction
			}, {
				where: {action: ctx.params.action}
			});
		} catch (err) {
			this.logger.error("Error occurred while updating latest modlog entry", err.name, err.message);
		}

		return true;
	}

	/**
	 * Gets the contents of the modqueue as logged in the database (as opposed to the modqueue as returned by Reddit). Used by `reddit-rt.modqueue`
	 * @function
	 * @package
	 * @static
	 * @name getOldModqueue
	 */
	async getOldModqueue(ctx: Context) {
		return await this.ModqueueEntry.findAll();
	}

	/**
	 * Adds a new modqueue entry to the database. Used by `reddit-rt.modqueue`
	 * @function
	 * @package
	 * @static
	 * @name addModqueueEntry
	 * @param {string} name - The fullname of the reported item
	 * @param {number} userReports - The number of user reports the item has
	 * @param {number} modReports - The number of mod reports the item has
	 */
	async addModqueueEntry(ctx: Context) {
		try {
			await this.ModqueueEntry.create({
				name: ctx.params.name,
				userReports: ctx.params.userReports || 0,
				modReports: ctx.params.modReports || 0
			});
		} catch (err) {
			this.logger.error("Error occurred while adding new modqueue entry", err.name, err.message);
		}
	}

	/**
	 * Updates a modqueue entry in the database. Used by `reddit-rt.modqueue`
	 * @function
	 * @package
	 * @static
	 * @name updateModqueueEntry
	 * @param {string} name - The fullname of the reported item
	 * @param {number} userReports - The number of user reports the item has
	 * @param {number} modReports - The number of mod reports the item has
	 */
	async updateModqueueEntry(ctx: Context) {
		try {
			await this.ModqueueEntry.update({
				userReports: ctx.params.userReports,
				modReports: ctx.params.modReports
			}, {
				where: {
					name: ctx.params.name
				}
			})
		} catch (err) {
			this.logger.error("Error occurred while updating modqueue entry", err.name, err.message);
		}
	}

	/**
	 * Removes a modqueue entry from the database. Used by `reddit-rt.modqueue`
	 * @function
	 * @package
	 * @static
	 * @name removeModqueueEntry
	 * @param {string} name - The fullname of the reported item
	 */
	async removeModqueueEntry(ctx: Context) {
		try { 
			await this.ModqueueEntry.destroy({
				where: {
					name: ctx.params.name
				}
			})
		} catch (err) {
			this.logger.error("Error occurred while updating modqueue entry", err.name, err.message);
		}
	}
}

module.exports = DatabaseService;