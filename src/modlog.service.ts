import { Service, Errors } from "moleculer";
import * as moment from "moment";
import * as yn from "yn";

/**
 * This module emits events whenever new modlog entries are created on Reddit. Event names follow the format `reddit-rt.modlog.<name>`, where `<name>` is one of `banuser`, `unbanuser`, `spamlink`, `removelink`, `approvelink`, `spamcomment`, `removecomment`, `approvecomment`, `addmoderator`, `invitemoderator`, `uninvitemoderator`, `acceptmoderatorinvite`, `removemoderator`, `addcontributor`, `removecontributor`, `editsettings`, `editflair`, `distinguish`, `marknsfw`, `wikibanned`, `wikicontributor`, `wikiunbanned`, `wikipagelisted`, `removewikicontributor`, `wikirevise`, `wikipermlevel`, `ignorereports`, `unignorereports`, `setpermissions`, `setsuggestedsort`, `sticky`, `unsticky`, `setcontestmode`, `unsetcontestmode`, `lock`, `unlock`, `muteuser`, `unmuteuser`, `createrule`, `editrule`, `deleterule`, `spoiler`, `unspoiler`, `modmail_enrollment`, `community_styling`, `community_widgets`, `markoriginalcontent` and the payload being the modlog item details.
 * 
 * While the `checkModlog` action is publicly exposed, it is not intended to be used by other packages. It is documented here for completeness.
 * 
 * @module "reddit-rt.modlog"
 * @version 1
 */
class ModlogService extends Service {
	protected checkInterval: number;

	constructor(broker) {
		super(broker);
		
		this.parseServiceSchema({
			name: "reddit-rt.modlog",
			version: 1,
			dependencies: [
				{name: "reddit.modlog", version: 1},
				{name: "reddit-rt.db", version: 1}
			],
			actions: {
				checkModlog: {
					name: "checkModlog",
					handler: this.checkModlog
				}
			},
			created: this.serviceCreated,
			// @ts-ignore
			started: this.serviceStarted
		});
	}

	async serviceCreated() {
		this.checkInterval = Number(process.env.REDDIT_RT_MODLOG_INTERVAL) ||  60*1000;
	}

	async serviceStarted() {
		if(yn(process.env.REDDIT_RT_USE_MODLOG, {default: false})){
			setInterval(() => this.broker.call("v1.reddit-rt.modlog.checkModlog"), this.checkInterval);
		}
	}

	/**
	 * Retrieves updates to the modlog on Reddit and emits events as needed. 
	 * @function
	 * @package
	 * @static
	 * @name checkModlog
	 */
	async checkModlog() {
		// Check to see what the latest modlog entries are
		let latestAction = await this.broker.call("v1.reddit-rt.db.getLatestModAction");
		let modlog;
		
		// Get the latest event and log to DB, don't emit
		if (latestAction === null) {
			modlog = await this.broker.call("v1.reddit.modlog.getModlog", {
				subreddit: process.env.REDDIT_RT_SUBREDDIT,
				limit: 1
			});

			await this.broker.call("v1.reddit-rt.db.updateModAction", {
				action: modlog[0].action,
				timestamp: modlog[0].created_utc,
				modAction: modlog[0].id
			});

			return
		} else {
			modlog = await this.broker.call("v1.reddit.modlog.getModlog", {
				subreddit: process.env.REDDIT_RT_SUBREDDIT,
				limit: 100,
				before: latestAction
			});

			for (let entry of modlog) {
				this.broker.emit(`reddit-rt.modlog.${entry.action}`, entry);

				await this.broker.call("v1.reddit-rt.db.updateModAction", {
					action: entry.action,
					timestamp: entry.created_utc,
					modAction: entry.id
				});
			}
		}
	}
}

module.exports = ModlogService;