import { Service, Errors } from "moleculer";
import * as EventSource from "eventsource";
import * as yn from "yn";

/**
 * This module emits events whenever new comments or submissions are published on the specified subreddit. The events emitted are `reddit-rt.posts.comment` and `reddit-rt.posts.submission`, with the payload being the post details.
 * 
 * This uses the Pushshift SSE stream as the data source, meaning that this module will not consume Reddit API calls. 
 * 
 * @module "reddit-rt.posts"
 * @version 2
 */
class SseService extends Service {
	protected enabled: boolean;
	protected allSubreddits: boolean;
	protected es: EventSource;

	constructor(broker) {
		super(broker);
		
		this.parseServiceSchema({
			name: "reddit-rt.posts",
			version: 2,
			created: this.serviceCreated,
			// @ts-ignore
			started: this.serviceStarted,
			// @ts-ignore
			stopped: this.serviceStopped
		});
	}

	async serviceCreated() {
		this.enabled = yn(process.env.REDDIT_RT_USE_POSTS, {default: false});
		this.allSubreddits = yn(process.env.REDDIT_RT_POSTS_ALLSUBREDDITS, {default: false});

		if(!process.env.REDDIT_RT_SUBREDDIT && !this.allSubreddits && this.enabled) throw new Errors.MoleculerError("You didn't supply a subreddit for the SSE stream, you dingus");
		if(this.allSubreddits) this.logger.warn("WARNING: Events will be emitted for all subreddits! Verify this is actually what you want.");
		
		if(this.enabled) {
			let sseUrl = `http://stream.pushshift.io/`;

			if(!this.allSubreddits) sseUrl += `?subreddit=${process.env.REDDIT_RT_SUBREDDIT}`;

			this.es = new EventSource(sseUrl);
			
			this.es.onopen = () => Promise.resolve();
		}
	}
	
	// TODO: Write logic to handle SSE stream disconnections/crashes and develop a fallback using the standard Reddit API
	async serviceStarted() {
		if (this.enabled) {
			await this.es.onopen;

			// I tried having this as a class method, but `this` would get reassigned to that of the 
			// EventSource object. Having it here keeps `this` as the class itself.
			const handler = event => {
				let data = JSON.parse(event.data);

				this.broker.emit(`reddit-rt.${event.type === "rc" ? "comment":"submission"}`, data);
			}
	
			this.es.addEventListener("rc", handler);
			this.es.addEventListener("rs", handler);
			
			if(this.allSubreddits) {
				this.logger.info("Now emitting events for all subreddits");
			} else {
				this.logger.info(`Now emitting events for /r/${process.env.REDDIT_RT_SUBREDDIT}`);
			}
		}
	}

	async serviceStopped() {
		if (this.enabled) {
			this.logger.info("Closing SSE stream...");
			this.es.close();
		}
	}
}

module.exports = SseService;