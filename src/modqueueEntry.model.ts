import * as Sequelize from "sequelize";

module.exports = function(sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes) {
	return sequelize.define("ModqueueEntry", {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
			unique: true
		},
		userReports: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		},
		modReports: {
			type: DataTypes.INTEGER,
			defaultValue: 0
		}
	})
}